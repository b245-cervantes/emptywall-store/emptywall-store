
import { Fragment } from 'react';
import Navbar from './components/Navbar';
import Login from './pages/Login';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Register from './pages/Register';
import Home from './pages/Home';


function App() {
  return (
    <Fragment>
        <Navbar />  
        <Router>
        <Routes>
        <Route path='/'  element ={<Home />} />
       <Route path='/register' element={ <Register />} />
       <Route path='/login' element= { <Login />} />
        </Routes>
        </Router>
    </Fragment>
    
  );
}

export default App;
