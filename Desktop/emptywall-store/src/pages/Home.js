import React, { Fragment } from 'react'
import Carousel from '../components/Carousel'

function Home() {
  return (
    <Fragment>
        <Carousel />
    </Fragment>
  )
}

export default Home