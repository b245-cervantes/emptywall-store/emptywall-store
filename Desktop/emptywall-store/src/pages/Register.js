import React from 'react'
import emptLogo from '../assets/logo2.png'

function Register() {
  return (
    <div className='container mx-auto w-full 100v'>     
        <div className='flex flex-col justify-center items-center'>
        <div>
            <img src={emptLogo} style={{width: '200px'}} />
        </div>
        <h2 className='text-2xl font-poppins -mt-11 mb-5'>Register Account</h2>
        <form className='flex flex-col w-[20rem]'>
            <input type='email' placeholder='Email address' id='email' className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none' required/>
            <input type='password' placeholder='Password' id='password' className='border border-gray-300 p-2 rounded-md font-poppins outline-none' required/>
            <input type='text' placeholder='First Name' id='firstName'  className='border border-gray-300 p-2 rounded-md font-poppins mt-5 outline-none' required/>
            <input type='text' placeholder='Last Name' id='lastName' className='border border-gray-300 p-2 rounded-md font-poppins mt-5 outline-none' required/>
            <input type='date' placeholder='Date of Birth' id='birthday' className='border text-gray-400 border-gray-300 p-2 rounded-md mt-5 font-poppins outline-none' required/>
            <input type='text' placeholder='Mobile Number' id='mobileNum' className='border border-gray-300 p-2 rounded-md font-poppins mt-5 outline-none' required/>
            <div className='radio-toolbar flex items-center justify-between space-x-[9px] mr-10 mt-5'>
                <input type='radio' id='male' value='male' name='gender' className='-mr-2' required/>
               
                <label for='male'>Male</label>
                <input type='radio' id='female' value='female' name='gender'/>
            
                <label for='female'>Female</label>
            </div>
            <h6 className='text-xs text-center text-gray-400 mt-2'>Get a Nike Member Reward every year on your Birthday.</h6>
            <h5 className='text-center text-xs mt-5'> By creating an account, you agree to Emptywall's <a className='underline underline-offset-2'>Privacy Policy</a> and <a className='underline underline-offset-2'>Terms of Use</a></h5>
            <button className='p-3 bg-black text-white rounded-md mt-5'>JOIN US</button>
            <h5 className='text-center text-xs mt-5 text-gray-400'>Already a Member? <a className='underline underline-offset-2 text-black'>Sign In</a></h5>
        </form>
        </div>
    </div>
  )
}

export default Register