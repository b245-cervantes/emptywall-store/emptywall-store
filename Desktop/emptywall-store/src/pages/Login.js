import React from 'react';
import emptLogo from '../assets/logo2.png';
import { Link }  from 'react-router-dom';
function Login() {
  return (
    <div className='container mx-auto w-full 100v'>     
        <div className='flex flex-col justify-center items-center'>
        <div>
            <img src={emptLogo} style={{width: '200px'}} />
        </div>
        <h2 className='text-2xl font-poppins -mt-11 mb-5'>Login Account</h2>
        <form className='flex flex-col w-[20rem]'>
            <input type='email' placeholder='Email address' className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none'/>
            <input type='password' placeholder='Password' className='border border-gray-300 p-2 rounded-md font-poppins outline-none'/>
            <h6 className='text-xs text-center mt-5 text-gray-400'>Forgotten your password?</h6>
            <h5 className='text-center text-xs mt-5'> By logging in, you agree to Emptywall's <a className='underline underline-offset-2'>Privacy Policy</a> and <a className='underline underline-offset-2'>Terms of Use</a></h5>
            <button className='p-3 bg-black text-white rounded-md mt-5'>SIGN IN</button>
            <h5 className='text-center text-xs mt-5 text-gray-400'>Not a Member? <Link as = {Link} to = '/register' className='underline underline-offset-2 text-black'>Join Us</Link></h5>
        </form>
        </div>
    </div>
  )
}

export default Login