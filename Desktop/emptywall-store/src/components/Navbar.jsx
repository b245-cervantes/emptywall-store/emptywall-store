import React from 'react';
import Logo from '../assets/logo.png';
import { BiSearchAlt , BiHeart, BiShoppingBag } from 'react-icons/bi';
import SaleBanner from './SaleBanner';
import { Fragment } from 'react';


function Navbar() {

  

  return (
    <div className='xl:container xl:mx-auto'>
      <Fragment>
      {/* SALE BANNER ON TOP */}
      <SaleBanner />

      {/* LOGO */}
      <nav className='w-full h-5 mt-5 flex justify-between items-center'>
        <div className='ml-10'>
          <img src={Logo} alt="Logo image" style={{width: '50px'}} />
        </div>
        {/* nav-links */}
        <div className='-mt-7 md:mr-[12%]'>
          <ul className='hidden lg:flex relative lg:absolute space-x-7'>
            <li>Men</li>
            <li>Women</li>
            <li>Customise</li>
            <li>Sale</li>
            <li>Caps</li>
          </ul>
        </div>
        {/* SEARCH BARS AND ICONS */}
       <form className='flex absolute md:absolute lg:relative w-70 right-0 mr-[15%] md:mr-[12%] lg:mr-[-25%] md:-mt-5 lg:mt-1'>
       <input type='text' placeholder='Search' name='search' className='bg-gray-100 text-left p-1 pl-10 w-[80%] rounded-full
         hidden lg:flex outline-none hover:bg-gray-300'/>
        <button type='submit' className='md:absolute lg:mt-[2px] md:-mt-[10] sm:mr-[29%] sm:right-0 md:mr-[85%]
        hover:bg-gray-300 p-[2px] rounded-full'><BiSearchAlt size={26} /></button>
       </form>
       <div className='flex space-x-3 mr-10'>
        <button className='hidden lg:flex'><BiHeart size={26}/></button>
        <button><BiShoppingBag size={26}/></button>
       </div>
      </nav>
    
      </Fragment>
      </div>

  )
  
}

export default Navbar

