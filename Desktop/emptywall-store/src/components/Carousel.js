import React, { useState } from 'react';
import { BsChevronCompactLeft, BsChevronCompactRight } from 'react-icons/bs';
import { RxDotFilled } from 'react-icons/rx';

function Carousel() {
  const slides = [
    {
      url: 'https://i.ibb.co/LdZ77GB/img1.png',
    },
    {
      url: 'https://i.ibb.co/4jgJ4GD/img2.png',
    },
    {
      url: 'https://i.ibb.co/fDyH8Bx/img3.png',
    },

    {
      url: 'https://i.ibb.co/N9bLngF/img4.png',
    },
    {
      url: 'https://i.ibb.co/Csxkcjs/img5.png',
    },
  ];

  const [currentIndex, setCurrentIndex] = useState(0);

  const prevSlide = () => {
    const isFirstSlide = currentIndex === 0;
    const newIndex = isFirstSlide ? slides.length - 1 : currentIndex - 1;
    setCurrentIndex(newIndex);
  };

  const nextSlide = () => {
    const isLastSlide = currentIndex === slides.length - 1;
    const newIndex = isLastSlide ? 0 : currentIndex + 1;
    setCurrentIndex(newIndex);
  };

  const goToSlide = (slideIndex) => {
    setCurrentIndex(slideIndex);
  };

  setTimeout(function(){
        if(currentIndex == 0){
            nextSlide();
        } else if(currentIndex == 1){
            nextSlide()
        } else if(currentIndex == 2){
            nextSlide()
        } else if(currentIndex == 3){
            nextSlide();
        } else {
            nextSlide();
        }
    },5000)

  return (
    <div className='max-w-[1550px] h-[780px] w-full m-auto py-16 px-4 relative group -mt-5'>
      <div
        style={{ backgroundImage: `url(${slides[currentIndex].url})` }}
        className='w-full h-full bg-center bg-cover duration-500'
      ></div>
       
        <button className='absolute top-[75%] left-[40%] md:left-[45%] bg-black text-white
        p-4 rounded-full font-poppins hover:bg-gray-600 ease-in-out duration-500 drop-shadow-2xl'>SHOW NOW</button>
       <img src="https://i.ibb.co/DKGYn1s/img6.png" alt="img6" border="0" className='absolute top-[5%]' style={{width:'200px'}}></img>
      {/* <div className='hidden group-hover:block absolute top-[50%] -translate-x-0 translate-y-[-50%] left-5 text-2xl rounded-full p-2 bg-black/20 text-white cursor-pointer'>
        <BsChevronCompactLeft onClick={prevSlide} size={30} />
      </div>

      <div className='hidden group-hover:block absolute top-[50%] -translate-x-0 translate-y-[-50%] right-5 text-2xl rounded-full p-2 bg-black/20 text-white cursor-pointer'>
        <BsChevronCompactRight onClick={nextSlide} size={30} />
      </div> */}
      <div className='flex top-4 justify-center py-2'>
        {slides.map((slide, slideIndex) => (
          <div
            key={slideIndex}
            onClick={() => goToSlide(slideIndex)}
            className='text-2xl cursor-pointer'
          >
            <RxDotFilled />
          </div>
        ))}
      </div>
    </div>
  );
}

export default Carousel;