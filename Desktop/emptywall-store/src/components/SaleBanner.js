import React from 'react'

function SaleBanner() {

    let word = document.getElementsByTagName('span');
  let i = 0;
  
   function rotator() {
     
      word[i].style.display = 'none';
      i = (i + 1) % word.length; 
      word[i].style.display = 'initial';
     
   }
        setInterval(rotator, 2000);

  return (
    <div>
        <div className='w-full h-[40px] bg-[#000000] md:flex justify-center items-center px-4 text-gray-300 hidden'>
            <span className='text-xs font-poppins' style={{display: 'initial'}}>TAKE AN ADDTIONAL 30% OFF SALE <a className='underline underline-offset-1'>SHOP NOW</a></span>
            <span className='text-xs font-poppins'>Free Shipping and Returns</span>
       </div>
      <div className='w-full h-40px mt-2 hidden md:inline-block'>
          <ul className='flex justify-end space-x-5 text-xs mr-10 font-poppins text-gray-500'>
            <li>About Us</li>
            <li className='border-x-[1px] border-gray-400 px-3'>Help</li>
            <li className=''>Register</li>
            <li className='border-l-[1px] border-gray-400 pl-3'>Sign In?</li>
          </ul>
      </div>
    </div>
  )
}

export default SaleBanner