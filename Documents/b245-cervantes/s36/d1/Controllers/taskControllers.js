const { response } = require('express')
const task = require('../Models/task')
const Task = require('../Models/task')


//Controllers and functions


module.exports.getAll = (req, res)=>{

    Task.find({}).then(result => {
        //to capture the result of the find method
        return res.send(result)
    })
    //.catch method captures the error when the find method is executed
    .catch(error => {
        return res.send(error)
    })
}

module.exports.createTask = (request, response) => {
    const input = request.body;

    Task.findOne({name: input.name})
    .then(result =>{
        if(result !== null){
            return response.send("The task is already existing!")
        }else{
            let newTask = new Task({
                name: input.name
            });

            newTask.save().then(save => {
                return response.send("The task is successfully added!")
            }).catch(error=>{
                return response.send(error)
            })
        }
    })
    .catch(error =>{
        return response.send(error)
    })
};

//Controller that will delete the document that contains

module.exports.deleteTask = (req,res) => {
    let idTobBeDeleted = req.params.id;
    

    //findByIdAndRemove - to find the document that contains the id and then delete the document
    Task.findByIdAndRemove(idTobBeDeleted)
    .then(result => {
        return res.send(result)
    })
    .catch(error => {
        return res.send(error)
    })
};