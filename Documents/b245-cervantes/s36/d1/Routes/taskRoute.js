const { application } = require('express');
const express = require('express');
const router = express.Router();

const taskController = require('../Controllers/taskControllers')



//routes

router.get('/', taskController.getAll)
router.post('/addTask', taskController.createTask)

//route for delete task

router.delete('/deleteTasks/:id',taskController.deleteTask)




module.exports = router;