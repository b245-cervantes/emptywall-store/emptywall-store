const express = require('express');
const mongoose = require('mongoose');
const mongodb = express.mongodb;

//mongoDb connection

mongoose.connect("mongodb+srv://admin:admin@batch245-cervantes.ttw2wvw.mongodb.net/s35-discussion?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error"))
db.once("open", ()=> console.log("We are connected to cloud database!"))

module.exports=mongodb