const express = require('express')
const port = 3001;
const app = express();
const mongodb = require('./mongodb');
const taskRoute = require('../Routes/taskRoute')

//middlewares

app.use(express.json())
app.use(express.urlencoded({extended:true}))


//routing

app.use("/tasks", taskRoute)

app.listen(port, ()=>
    console.log(`The Server is running at port ${port}`)
)

/**
 * Separation of concerns:
 *   Model should be connected to the controller.
 *   Controller should be connected to the Routes.
 *   Routes should be connected to the server/application
 */