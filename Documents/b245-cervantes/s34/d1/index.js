
//load the expressjs module into our application and save it in a variable
//called express
const express = require('express');

//localhost port number
const port = 3030;

//app is our server
//create an application that uses and stores it as app.
const app = express();

//middleware
//express.json() is a method which allow us to handle the streaming of daya and
//automatically parse the incoming json from our request.
app.use(express.json())

//mockdaya
let users = [
    {
        username:"itskennot",
        email: "magandanglalaki@gmail.com",
        password: "okaylangmasaktan"
    },
    {
        username: "thorThundar",
        email: "iloveyou@gmail.com",
        password: "stormBreaker"
    }
]

//express has methods to use as routes corresponding to HTTP methods
//Syntax:
//app.method(<endpoint>, function for request and response)

//[http method GET]
   app.get("/" ,(req, res) => {
    //res.status = writeHead
    //response.send = write with end()
    res.status(201).send("Hello from express")
   })
   //Mini Activity: 
   // create a "Get" route in express js which will be able to send a message 
   // in the client:
   //endpoint: /greeting
   //message: "Hello from batch245-cervantes"
   //code: 201

   app.get("/greeting",(req, res) => {
    res.status(201).send("Hello from Batch245-cervantes")
   })

    app.get('/users', (req,res) => {
        res.send(users);
    })
   //[HTTP method for POST]

   app.post('/users', (req, res) => {
     let input = req.body;
     let newUser = {
        username : input.username,
        email: input.email,
        password: input.password
     }
     users.push(newUser);
     res.send(`The ${newUser.username} is now registered in our website with email : ${newUser.email}`);
   })

   

   //[HTTP method DELETE]

   app.delete('/users', (req,res)=>{
    let deletedUser = users.pop()
    res.send(deletedUser);
   })

   //[HTTP method PUT]
   app.put('/users/:index', (req,res) => {
     let indexToBeUpdated = req.params.index
     console.log(typeof indexToBeUpdated)
     indexToBeUpdated = parseInt(indexToBeUpdated)
     if(indexToBeUpdated < users.length) {
        users[indexToBeUpdated].password = req.body.password
        res.send(users[indexToBeUpdated])
     } else {
        res.status(404).send("Page cannot be found!. 404!")
     }
     
   })

   app.listen(port, () => console.log(`The server is running at port ${port}`));